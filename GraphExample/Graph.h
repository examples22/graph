#pragma once
#include "stdafx.h"
#include <iostream>

using namespace std;

template <class T>
class Graph
{
public:
	struct Vertex
	{	
		typedef pair<double, Vertex*> Edge;
		T info;
		list<Edge> adjacency;
		bool visited; // for graph travesal
		Vertex * prev; // for shortest path
		double dist; // for shortest path
		
      Vertex( const T & item ) : info( item ) { visited = false; prev = NULL; };
		void AddEdge( const Edge & e ) { adjacency.push_back( e ); }
		void RemoveEdge( Edge & e ) { adjacency.remove( e ); }
		bool operator< ( Vertex other ) { return dist < other.dist; }
		bool operator> ( Vertex other ) { return dist > other.dist; }
	};

   // for minimum priority_queue of vertices
	struct CompareV
	{
		bool operator() ( const Vertex * lhs, const Vertex * rhs ) const
		{
			return lhs->dist > rhs->dist ;
		}
	};
	
private:
	vector<Vertex*> vertices;
	
	// return the index of item if it is in the graph. Otherwise return -1.
	int Find( const T & item ) const
	{
		for ( int i = 0; i < (int) vertices.size(); i++ )
			if ( vertices[i]->info == item )
				return i;
		return -1;
	}

	// reset the visited and prev marks
	void ClearSearch()
	{
		for ( int i = 0; i < (int)vertices.size(); i++ )
		{
			vertices[i]->visited = false;
			vertices[i]->prev = NULL;
		}
	}

public:
	// destructor
	virtual ~Graph()
	{
		for ( int i = 0; i < (int) vertices.size(); i++ )
			delete vertices[i];
		vertices.clear();
	}

	// read access to the list of vertices
	vector<Vertex*> Vertices()
	{
		return vertices;
	}

	// add a new node to the graph if item is not in the graph yet. 
	void AddVertex( const T & item )
	{
		if ( Find( item ) == -1 )
			vertices.push_back( new Vertex( item ) );
	}

	// add a new edge to the graph
	void AddEdge( const T & from, const T & to, const double & weight )
	{
		int indexFrom = Find( from );
		int indexTo = Find( to );
		if ( indexFrom == -1 )
		{
			AddVertex( from );
			indexFrom = (int)vertices.size() - 1;
		}
		if ( indexTo == -1 )
		{
			AddVertex( to );
			indexTo = (int)vertices.size() - 1;
		}
		pair<double, Vertex*> edge = make_pair( weight, vertices[indexTo] );
		vertices[indexFrom]->AddEdge( edge );
	}

	// remove an existing edge from the graph.
	void RemoveEdge( const T & from, const T & to, const double & weight )
	{
		int indexFrom = Find( from );
		int indexTo = Find( to );
		if ( indexFrom != -1 && indexTo != -1 )
		{
			pair<double, Vertex*> edge = make_pair( weight, vertices[indexTo] );
			vertices[indexFrom]->RemoveEdge( edge );
		}
	}

	// remove an existing node from the graph.
	void RemoveVertex( const T & item )
	{
		int index = Find( item );
		if ( index != -1 )
		{
			// first delete all incomming edges to the vertex of item
			list<Vertex::Edge>::iterator iter;
			for( int i = 0; i < (int)vertices.size(); i++ )
			{
				iter = ( vertices[i]->adjacency ).begin();
				for(; iter != ( vertices[i]->adjacency ).end(); iter++ )
					if ( iter->second->info == item )
					{
						vertices[i]->adjacency.erase( iter );
						break;
					}
			}

			// then delete the vertex of item
			vector<Vertex*>::iterator iterV = vertices.begin() + index;
			delete *iterV;
			vertices.erase( iterV );
		}		
	}

	// Print the graph: for each node, list all nodes it can reach and the edge weight.
	void Print( ostream & outFile ) const
	{
		list<Vertex::Edge>::iterator iter;
		for ( int i = 0; i < (int)vertices.size(); i++ )
		{
			outFile << vertices[i]->info << " ";
			outFile << (vertices[i]->adjacency).size() << endl;			
			for ( iter = (vertices[i]->adjacency).begin(); 
				   iter != (vertices[i]->adjacency).end(); 
					iter++ )
				outFile << iter->first << " " 
				        << (iter->second)->info << endl;
		}
	}

	// Print the path found by different search methods
	// 'd' for DFS; 'b' for BFS; 's' for shortest path
	void PrintPath ( ostream & outFile, const T & from, const T & to, char searchMethod )
	{
		deque<Vertex*> path;
		Vertex* v;
		double totalWeight = 0;
		bool found = false;
		
		if ( searchMethod == 'd' )
			found = DepthFirstSearch( from, to, path );
		else if ( searchMethod == 'b' )
			found = BreadthFirstSearch( from, to, path );
		else if ( searchMethod == 's' )
		{
			ShortestPath ( from );
			int index = Find(to);
			if ( index != -1 )
				v = vertices[index];
			path.push_back(v);
			while (v->prev != v)
			{
				path.push_front(v->prev);
				v = v->prev;
			}
			found = true;
		}
		
		if (found)
		{
			for (int i = 0; i < (int)path.size(); i++)
				outFile << path[i]->info << "-->";
			outFile << "DONE!" << endl;
		}
		else
		{
			outFile << "Cannot find a path!" << endl;
			return;
		}
	}

	// generate a path from "from" to "to" nodes by depth first search
	bool DepthFirstSearch( const T & from, const T & to, 
		                    deque<Vertex*> & path ) 
	{
		stack<Vertex*> st;
		Vertex * v;
		list<Vertex::Edge>::iterator iter;

		path.clear();
		int indexFrom = Find( from );
		int indexTo = Find( to );
		if ( indexFrom == -1 || indexTo == -1 )
			return false;

		bool found = false;
		ClearSearch();
		vertices[indexFrom]->visited = true;
		st.push( vertices[indexFrom] );
		while ( ! found && ! st.empty() )
		{
			v = st.top();
			st.pop();
			path.push_back( v );
			found = ( v->info == to );
			if ( ! found )
				for ( iter = v->adjacency.begin(); 
						iter != v->adjacency.end(); iter++ )
					if ( iter->second->visited == false ) 
					{
						iter->second->visited = true;
						st.push( iter->second );
					}
		}
		return found;
	}

	// generate a path from "from" to "to" nodes by breath first search
	bool BreadthFirstSearch( const T & from, const T & to, 
		                    deque<Vertex*> & path ) 
	{
		queue<Vertex*> qu;
		Vertex * v, * prev;
		list<Vertex::Edge>::iterator iter;
		int count = 0;

		path.clear();
		int indexFrom = Find( from );
		int indexTo = Find( to );
		if ( indexFrom == -1 || indexTo == -1 )
			return false;

		bool found = false;
		ClearSearch();
		qu.push( vertices[indexFrom] );
		vertices[indexFrom]->visited = true;
		while ( ! found && ! qu.empty() )
		{
			v = qu.front();
			qu.pop();
			prev = v;
			found = ( v->info == to );
			if ( ! found )
				for ( iter = v->adjacency.begin(); 
						iter != v->adjacency.end(); iter++ )
				{
					if ( iter->second->visited == false )
					{
						iter->second->visited = true;
						iter->second->prev = v;
						qu.push( iter->second );
					}
				}
		}
		if ( found )
		{
			Vertex * v = vertices[indexTo];
			path.push_front( v );
			while ( v->prev != NULL )
			{
				path.push_front( v->prev );
				v = v->prev;
			}
		}
		return found;
	}	

	// print the result of single source shortest path search from the "from" node
	void ShortestPath ( const T & from )
	{
		priority_queue<Vertex*, vector<Vertex*>, CompareV> pq;
		Vertex * v;
		list<Vertex::Edge>::iterator iter;
		double minDist;
		
		ClearSearch();

		int indexFrom = Find( from );
		if ( indexFrom == -1 )
			return;
		v = vertices[indexFrom];
		v->dist = 0;
		v->prev = v;
		v->visited = true;
		pq.push( v );
		cout << "Last Vertex	Destination	Distance" << endl;
		cout << "------------------------------------------------" << endl;

		while ( !pq.empty() )
		{
			v = pq.top();
			pq.pop();
			cout << v->prev->info << "\t\t" << v->info << "\t\t" << v->dist << endl;
			minDist = v->dist;
			for ( iter = v->adjacency.begin(); 
					iter != v->adjacency.end(); iter++ )
			{
				if ( iter->second->visited == false )
				{
					iter->second->visited = true;
					iter->second->prev = v;
					iter->second->dist = minDist + iter->first;
					pq.push( iter->second );
				}					
			}
		}
	}

};

